<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Información del Autor</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $id_a = $_GET['id'];
  $error = false;
  if (empty($id_a)) {
    $error = true;
?>
  <p>Error, no se ha indicado el ID del Autor</p>
<?php
  } 
  else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select id_autor, nombre_autor
      from biblioteca.autor
      where id_autor = '".$id_a."';";

    $autor = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($autor) == 0) {
      $error = true;
?>
  <p>No se ha encontrado algún Autor con ID <?php echo $id_a; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($autor, null, PGSQL_ASSOC);
      $nombre_autor = $tupla['nombre_autor'];
?>
<table>
  <caption>Información del Autor</caption>
  <tbody>
    <tr>
      <th>ID</th>
      <td><?php echo $id_a; ?></td>
    </tr>
    <tr>
      <th>NOMBRE</th>
      <td><?php echo $nombre_autor; ?></td>
<?php
    }
  }
?>
    </tr>
  </tbody>
</table>

<?php
  pg_free_result($result);
  pg_close($dbconn);

  if (!$error) {
?>
<form action="delete-autor.php" method="post">
  <input type="hidden" name="id" value="<?php echo $id_a; ?>" />
  <p>¿Está seguro/a de eliminar este Autor?</p>
  <input type="submit" name="submit" value="DELETE" />
  <p>
    Se borrarán a su vez todos los libros y ejemplares en existencia relacionados con este Autor.
  </p>
</form>

<form action="autores.php" method="post">
  <input type="submit" name="submit" value="Cancelar" />
</form>
<?php
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="autores.php">Lista de libros</a></li>
</ul>

</body>
</html>