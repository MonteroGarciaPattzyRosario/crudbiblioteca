<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Información de ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $clave_ejemplar = $_GET['clave_ejemplar'];

  if (empty($clave_ejemplar)) {
?>
  <p>Error, no se ha indicado el clave_ejemplar del ejemplar</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select clave_ejemplar, conservacion_ejemplar, isbn
      from biblioteca.ejemplar
      where clave_ejemplar = '".$clave_ejemplar."';";

    $ejemplar = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($ejemplar) == 0) {
?>
  <p>No se ha encontrado algún Ejemplar con clave_ejemplar <?php echo $clave_ejemplar; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($ejemplar, null, PGSQL_ASSOC);
      $conservacion_ejemplar = $tupla['conservacion_ejemplar'];
	  $isbn = $tupla['isbn'];
?>
<table>
  <caption>Información de ejemplar</caption>
  <tbody>
    <tr>
      <th>Clave Ejemplar</th>
      <td><?php echo $clave_ejemplar; ?></td>
    </tr>
    <tr>
      <th>Conservacion Ejemplar</th>
      <td><?php echo $conservacion_ejemplar; ?></td>
    </tr>
	<tr>
      <th>Conservacion ISBN</th>
      <td><?php echo $isbn; ?></td>
    </tr>
    <tr>
      <th>Libro</th>
      <td>
<?php
      $query = "select titulo_libro
        from biblioteca.libro as L
          where L.isbn = '".$isbn."';";

      $libros = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($libros) == 0) {
?>
        <p>Sin Informacion del libro</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($libros, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
          <li><?php echo $atributo; ?></li> 
<?php
          }
        }
?>
        </ul>
<?php
      }
?>
    </tr>
    <tr>
      <th>Autor/es</th>
      <td>
<?php
      $query = "select nombre_autor
        from biblioteca.autor as A inner join biblioteca.libro_autor as LA on
		A.id_autor=LA.id_autor
        where LA.isbn = '".$isbn."';";

      $ejemplares = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($ejemplares) == 0) {
?>
        <p>Sin Autor/es</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($ejemplares, null, PGSQL_ASSOC)) 
		{
          $nombre_autor = $tupla['nombre_autor'];
?>
          <li><?php $nombre_autor; ?></li> 
<?php
        }
?>
        </ul>
<?php
      }
    }
  }
?>
    </tr>
  </tbody>
</table>

<?php
  pg_free_result($result);
  pg_close($dbconn);
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplar.php">Lista de ejemplars</a></li>
</ul>

</body>
</html>
