<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de Ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $clave_ejemplar = $_GET['clave_ejemplar'];
  if (empty($clave_ejemplar)) 
  {
?>
  <p>Error, no se ha indicado la clave del Ejemplar</p>
<?php
  } 
  else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select clave_ejemplar, conservacion_ejemplar, isbn
      from biblioteca.Ejemplar
      where clave_ejemplar = '".$clave_ejemplar."';";

    $Ejemplar = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($Ejemplar) == 0) {
?>
  <p>No se ha encontrado algún Ejemplar con clave_ejemplar <?php echo $clave_ejemplar; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($Ejemplar, null, PGSQL_ASSOC);
      $conservacion_ejemplar = $tupla['conservacion_ejemplar'];
      $isbn = $tupla['isbn'];
?>
<form action="update-ejemplar.php" method="post">
<table>
  <caption>Información de Ejemplar</caption>
  <tbody>
    <tr>
      <th>Cleve Ejemplar</th>
      <td><input type="text" name="clave_ejemplar" value="<?php echo $clave_ejemplar; ?>" /></td>
    </tr>
    <tr>
      <th>Conservacion Ejemplar</th>
      <td><textarea name="conservacion_ejemplar"><?php echo $conservacion_ejemplar; ?></textarea></td>
    </tr>
	<tr>
      <th>ISBN</th>
      <td><input type="text" name="isbn" value="<?php echo $isbn; ?>"/></td>
    </tr>
  </tbody>
</table>
<input type="submit" name="submit" value="UPDATE" />
</form>
<?php
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplar.php">Lista de Ejemplars</a></li>
</ul>

</body>
</html>
