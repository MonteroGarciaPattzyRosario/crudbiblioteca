<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de Autor</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $id = $_GET['id'];

  if (empty($id)) {
?>
  <p>Error, no se ha indicado el ID del Autor</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select id_autor, nombre_autor
      from biblioteca.autor
      where id_autor = '".$id."';";

    $autor = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($autor) == 0) {
?>
  <p>No se ha encontrado algún autor con ID <?php echo $id; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($autor, null, PGSQL_ASSOC);
      $nombre_autor = $tupla['nombre_autor'];
?>
<form action="update-autor.php" method="post">
<table>
  <caption>Información del Autor</caption>
  <tbody>
    <tr>
      <th>ID</th>
      <td><input type="text" name="id" value="<?php echo $id; ?>" /></td>
    </tr>
    <tr>
      <th>NOMBRE</th>
      <td><textarea name="nombre"><?php echo $nombre_autor; ?></textarea></td>
    </tr>
  </tbody>
</table>
<input type="submit" name="submit" value="UPDATE" />
</form>
<?php
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="autores.php">Lista de libros</a></li>
</ul>

</body>
</html>
